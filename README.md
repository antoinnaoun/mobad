**Hello!**

This is Antoine Aoun Lead developer at i-magineworks and this document will let you know all what you need for MobAd.

From showing how the MobAd SDK behaves on the UI/UX level to guiding you through the journey to integrate with us.

1.  **MobAd UX:**

In the below link you may find videos that show how MobAd SDK behaves and some guides like how to create ads from our Content Management System and etc...  (The link is restricted now, kindly send me a request to access)

https://drive.google.com/drive/u/0/folders/1-9I-1EL7s30ZtQgRWOddxDg_Sn4Y8K8s

2. **MobAd Permissions:** 

> **For Android**

It is important to note that we have two variations of the SDK that differ very much as of permissions, they are the: normal variation, and the one with minimal permissions.

_Displayed for users:_

Normal SDK:
>     Notification permission (For notification Ads)
>     Draw over other apps (For bubble Ads)
>     Read phone state (For end of phone call detection)

Minimal SDK:
>     No permissions displayed for users at all

_Listed in android studio:_

Normal SDK:

    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />

    <uses-permission android:name="android.permission.INTERNET" />

    <uses-permission android:name="android.permission.READ_PHONE_STATE" />

    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />

    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <uses-permission android:name="android.permission.GET_ACCOUNTS"/>

    <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />

    <uses-permission android:name="com.google.android.gms.permission.AD_ID" />

 Minimal SDK:
 
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />

    <uses-permission android:name="android.permission.INTERNET" />

    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <uses-permission android:name="com.google.android.gms.permission.AD_ID" />   

> **For iOS**

_Displayed for users:_

    > Notification permission (For notification Ads)
    > Location (Optional)


_Listed in xcode:_

   ** For notification:**

    <key>UNNotificationExtensionCategory</key>

    <string>MobAdSDKNotificationCategoryGeneric</string>

    <key>UNNotificationExtensionDefaultContentHidden</key>
    <true/>

    <key>UNNotificationExtensionInitialContentSizeRatio</key>

    <real>0.1</real>

   ** For location which is optional:**

    <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>

    <string>This app requires location data. It works best when access is allowed in the background.</string>

    <key>NSLocationWhenInUseUsageDescription</key>

    <string>This app requires location data. It works best when access is allowed in the background.</string>

3. **MobAd Integration:**

Now, when you want to integrate with our SDK, please choose the corresponding guide that fits your application:

If your application is built in:

- **Android Native**

 https://gitlab.com/imw-developers/mobad-sdk-android

- **iOS Native Swift**

 https://gitlab.com/imw-developers/mobad-sdk-ios

- **iOS Native Objc**

 https://gitlab.com/imw-developers/mobad-sdk-ios-objc

- **React Native** 

 https://gitlab.com/imw-developers/mobad-react-native-integration

- **Flutter**  (Coming Soon)



If you still have any ambiguity or need support, feel free to contact us!
